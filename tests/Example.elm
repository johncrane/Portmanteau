module Example exposing (..)

import Expect exposing (equal)
import Test exposing (..)


suite : Test
suite =
    describe "Stub for tests"
        [ test "Stub" <|
            \_ ->
                Expect.equal 0 (List.length [])
        ]
